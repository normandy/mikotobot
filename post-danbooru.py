#!/usr/bin/env python3
import requests 
import sys

import os.path as op

from common import load_config
from mastodon import Mastodon

# --------------------------------------------------

def main():
    config = load_config()

    mastodon = Mastodon(
        access_token = 'token.dat',
        api_base_url = config['api_base_url']
    )

    URL = "https://danbooru.donmai.us/posts.json"
    LIMIT = 10
    MIN_SCORE = 25
    SAFETY = 's'
    TAGS_POST = config['danbooru']['tags_post']
    TAGS_FORBID = config['danbooru']['tags_forbid']
    TAGS_SENSITIVE = config['danbooru']['tags_sensitive']

    PARAMS = { 'tags': TAGS_POST.join(' '),
               'limit': LIMIT,
               'random': True } 

    print('[start] Settings:')
    print('LIMIT = ' + str(LIMIT) + ' | MIN_SCORE = ' + str(MIN_SCORE) + ' | SAFETY = ' + SAFETY)
    print('TAGS_POST=' + str(TAGS_POST))
    print('TAGS_FORBID=' + str(TAGS_FORBID))
    print('TAGS_SENSITIVE=' + str(TAGS_SENSITIVE) + '\n')

# --------------------------------------------------

    counter = 1
    b_search = True
    while b_search:
        r = requests.get(url = URL, params = PARAMS)
        print('[get] Attempt N' + str(counter) + '.')
        data = r.json()
        for i in range(0, LIMIT):
            fileurl = data[i]['file_url']
            print('url ', fileurl)
            fileid = data[i]['id']
            print('id ', fileid)
            filescore = data[i]['fav_count']
            print('score ', filescore)
            filesafe = data[i]['rating']
            print('rating ', filesafe)
            filetagstring = data[i]['tag_string']
            print('tags ', filetagstring)
            pulledtags = filetagstring.split()

            if (filesafe == SAFETY and filescore >= MIN_SCORE
                and not set(pulledtags).intersection(TAGS_FORBID)):
                print('[success] Found!')
                b_search = False
                break

# --------------------------------------------------

    # TODO: replace this with a libmagic call like in post-local.py
    fformat = op.splitext(fileurl)[1][1:]
    if (fformat == 'jpg'):
        fformat = 'jpeg'

    media = mastodon.media_post(requests.get(fileurl).content, f'image/{fformat}')
    toot  = f'https://danbooru.donmai.us/posts/{fileid}'

    b_sensetive = bool(set(pulledtags).intersection(TAGS_SENSITIVE))

    if (b_sensetive):
        print('[success] Marked as sensitive.')

    mastodon.status_post(toot, media_ids=[media], visibility=config['visibility'], sensitive=b_sensetive)
    print('[success] Posted!\n----------------------------------\n')

if __name__ == '__main__':
    sys.exit(main())
