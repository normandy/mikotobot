import os
import toml

def load_config() -> dict:
    """
    Return a dictionary containing the config from config.toml.
    """
    with open("config.toml", "r") as f:
        config = toml.load(f)
    return config
