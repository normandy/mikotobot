#!/usr/bin/env python3
import magic
import sqlite3
import sys
import time
import random
import os
import os.path as op

from contextlib import AbstractContextManager
from common import load_config
from datetime import datetime, timedelta
from typing import Any, Dict, Optional
from mastodon import Mastodon, MastodonError


class ArtDB(AbstractContextManager):
    conn: sqlite3.Connection

    def __init__(self, db_file: str) -> None:
        self.conn = sqlite3.connect(db_file, detect_types=sqlite3.PARSE_DECLTYPES)
        self.conn.row_factory = sqlite3.Row
        self.conn.execute('''CREATE TABLE IF NOT EXISTS art(
                                filename TEXT PRIMARY KEY,
                                last_used timestamp
                            )''')
        self.conn.commit()

    def __enter__(self) -> 'ArtDB':
        return self

    def __exit__(self, exc_type, exc_value, traceback) -> None:
        self.conn.close()

    def get_art(self, art: str) -> Optional[sqlite3.Row]:
        info = self.conn.execute('SELECT * FROM art WHERE filename=?', (art,)).fetchone()
        return info

    def update_art(self, art: str) -> None:
        self.conn.execute('''INSERT INTO art(filename, last_used) VALUES (:art, :last_used)
                ON CONFLICT(filename) DO UPDATE SET last_used=:last_used''',
                {'art': art, 'last_used': datetime.now()})
        self.conn.commit()

# --------------------------------------------------

def art_recently_used(art_info: Optional[sqlite3.Row], within: timedelta) -> bool:
    if art_info is None:
        return False
    last_used = art_info['last_used']
    return datetime.now() - last_used <= within

def main():
    config = load_config()

    DIR_SFW  = config['local']['dir_sfw']
    DIR_NSFW = config['local']['dir_nsfw']
    
    mastodon = Mastodon(
        access_token = 'token.dat',
        api_base_url = config["api_base_url"]
    )

    sfwcount  = len([name for name in os.listdir(DIR_SFW)  if op.isfile(op.join(DIR_SFW,  name))])
    nsfwcount = len([name for name in os.listdir(DIR_NSFW) if op.isfile(op.join(DIR_NSFW, name))])
    
    random_choice = random.randint(1, sfwcount + nsfwcount)
    print(f'\ns:{sfwcount} n:{nsfwcount} r:{random_choice}')
    
    is_safe = False if random_choice < nsfwcount else True
    art = ""
    
    dir_ = DIR_SFW if is_safe else DIR_NSFW
    files = [f for f in os.listdir(dir_) if op.isfile(op.join(dir_, f))]
    with ArtDB('art.db') as db:
        while True:
            art = op.join(dir_, random.choice(files))
            art_info = db.get_art(art)
            if not art_recently_used(art_info, timedelta(hours=5)):
                db.update_art(art)
                break
            else:
                print(f"Art {art} was used in the last 5 hours, trying again")

    fformat = magic.from_file(art, mime=True)

    with open(art, 'rb') as picture:
        data = picture.read()

    tries = 10
    for attempt in range(tries):
        try:
            media = mastodon.media_post(data, fformat)
            toot  = config["local"]["toot_text"]
            mastodon.status_post(toot, media_ids=[media], visibility=config["visibility"], sensitive=not is_safe)
        except MastodonError:
            print(f'Retrying again ({attempt + 1} of 10)')
            time.sleep(1)
            if attempt < tries - 1:
                continue
            else:
                raise
        break
    print(str(datetime.now()) + ': ' + art)

if __name__ == '__main__':
    sys.exit(main())
