#!/usr/bin/env python3
from mastodon import Mastodon
import argparse

if __name__ == '__main__':
    ap = argparse.ArgumentParser(
            description='Generate Mastodon login credentials')
    ap.add_argument('--client-name', '-n', help='Name of the app')
    ap.add_argument('--api-base-url', '-a', help='Instance URL')
    ap.add_argument('--username', '-u', help='Login username')
    ap.add_argument('--password', '-p', help='Login password')

    args = ap.parse_args()

    client_id, client_secret = Mastodon.create_app(
            client_name=args.client_name,
            api_base_url=args.api_base_url)
    mastodon = Mastodon(client_id=client_id, client_secret=client_secret,
            api_base_url=args.api_base_url)
    mastodon.log_in(args.username, args.password, to_file='token.dat')

    print('Login credentials saved to token.dat')
