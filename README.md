**NOTE: This project is officially dead. I've basically rewritten it in Go here: https://codeberg.org/normandy/fedi-picbot**

# mikotobot

Another bot for posting arts to your timeline. 

Currently posting on: https://biribiri.dev/mikotobot

Based on https://dev.udongein.xyz/NaiJi/udonge-bot

## Features
- Repost detection - will not repost anything that has already been posted within a specified amount of time in the past (Default: 5 hours).

## Initial setup
This bot has been tested on Python 3.7, 3.8 and 3.9. Other versions are not guaranteed to work.

We support you to create a virtualenv to not pollute your system with modules:

```shell
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

To generate the needed login credentials, use the helper script `gen-token.py` as follows (replace with appropriate values):
```shell
python3 gen-token.py \
	--client-name "mikotobot" \
	--api-base-url "https://biribiri.dev" \
	--username "mikotobot" \
	--password "asecretpassword"
```

This will create a `token.dat` file that will be used by the bot to login to your instance.

Copy `config.sample.toml` to `config.toml` and edit it as needed. Edit the `api_base_url` variable in `config.toml` sources. It must contain the url of the instance your bot is going to post on.

### `post-local.py`

Allows you to post random arts from your local `/sfw` and `/nsfw` folder.

* Create `sfw` and `nsfw` folders in the same location with the `post-local.py` script.
* If you want to download a huge chunk of arts to your folder first, in `download.py` edit global variable `TAGS`, write in there 2 tags you need. Now run
```bash
python3 download.py
```
* Copy `config.sample.toml` to `config.toml` and edit it as needed.
* To make a single post, now run
```bash
python3 post-local.py
```

### `post-danbooru.py`

Allows you to repost random arts from danbooru.donmai.us

* Copy `config.sample.toml` to `config.toml` and edit it as needed.
* To make a single post, now run
```bash
python3 post-danbooru.py
```
